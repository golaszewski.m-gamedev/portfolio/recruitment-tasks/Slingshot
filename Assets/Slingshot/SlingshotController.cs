﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

using Slingshot.Input;

namespace Slingshot
{
    public class SlingshotController : MonoBehaviour
    {
        [SerializeField]
        private ProjectileController projectilePrefab;
        private ProjectileController currentProjectile;

        [SerializeField]
        private float maxCancelDistance;
        [SerializeField]
        private float maxDrawDistance;

        [SerializeField]
        private float delayAfterStop;

        private bool projectileLoaded = true;
        private bool slingshotDrawn = false;

        private ProjectilePoolManager projectilePool;

        private void Awake()
        {
            projectilePool = GetComponent<ProjectilePoolManager>();
        }

        private void Start()
        {
            LoadNewProjectile();
        }

        private void OnEnable()
        {
            InputManager.Instance.InputActions.Gameplay.DrawSlingshot.started += DrawSlingshotCallback;
            InputManager.Instance.InputActions.Gameplay.DrawSlingshot.performed += ReleaseSlingshotCallback;
        }

        private void OnDisable()
        {
            InputManager.Instance.InputActions.Gameplay.DrawSlingshot.started -= DrawSlingshotCallback;
            InputManager.Instance.InputActions.Gameplay.DrawSlingshot.performed -= ReleaseSlingshotCallback;
        }

        private void DrawSlingshotCallback(InputAction.CallbackContext context)
        {
            if (!projectileLoaded) 
            {
                return;
            }

            Vector3 pointerPosition = Camera.main.ScreenToWorldPoint(Pointer.current.position.ReadValue());
            pointerPosition.z = 0;

            if (ActionWasInRadius(pointerPosition, maxDrawDistance))
            {
                slingshotDrawn = true;
                StartCoroutine(UpdateDraw());
            }
        }

        private void ReleaseSlingshotCallback(InputAction.CallbackContext context)
        {
            if (!projectileLoaded)
            {
                return;
            }

            Vector3 pointerPosition = Camera.main.ScreenToWorldPoint(Pointer.current.position.ReadValue());
            pointerPosition.z = 0;

            if (ActionWasInRadius(pointerPosition, maxCancelDistance))
            {
                // cancel shot
                ResetSlingshot();
            }
            else
            {
                // shoot projectile
                FireProjectile();
            }

            slingshotDrawn = false;
        }

        private bool ActionWasInRadius(Vector3 eventPosition, float checkRadius)
        {
            return (eventPosition - transform.position).magnitude <= checkRadius;
        }

        private IEnumerator UpdateDraw()
        {
            while (slingshotDrawn)
            {
                DrawSlingshot();
                yield return new WaitForEndOfFrame();
            }
        }

        private void ResetSlingshot()
        {
            currentProjectile.transform.position = transform.position;
        }

        private void DrawSlingshot()
        {
            Vector3 pointerPosition = Camera.main.ScreenToWorldPoint(Pointer.current.position.ReadValue());
            pointerPosition.z = 0;

            //calculate draw point, within max radius
            Vector3 direction = pointerPosition - transform.position;

            // trim if pointer exits max draw radius
            if (direction.magnitude > maxDrawDistance) 
            {
                direction *= (maxDrawDistance / direction.magnitude);
            }

            currentProjectile.transform.position = transform.position + direction;
        }

        private void LoadNewProjectile()
        {
            projectileLoaded = true;
            CameraController.Instance.SwitchFollowTarget(transform);
            currentProjectile = projectilePool.GetProjectileFromPool();
            currentProjectile.transform.position = Vector3.zero;
            currentProjectile.MakeStatic();
        }

        private void FireProjectile()
        {
            projectileLoaded = false;
            CameraController.Instance.SwitchFollowTarget(currentProjectile.transform);
            currentProjectile.HurlProjectile(transform.position - currentProjectile.transform.position * 10);
            StartCoroutine(WaitForProjectileStop(currentProjectile));
        }

        private IEnumerator WaitForProjectileStop(ProjectileController projectile)
        {
            bool projectileStopped = false;
            float stopVelocity = 0.5f;

            while (!projectileStopped)
            {
                yield return new WaitUntil(() => projectile.ProjectileRigidbody.velocity.magnitude < stopVelocity);
                yield return new WaitForSeconds(delayAfterStop);

                if (projectile.ProjectileRigidbody.velocity.magnitude < stopVelocity)
                {
                    projectileStopped = true;
                }
            }

            LoadNewProjectile();
        }
    }
}