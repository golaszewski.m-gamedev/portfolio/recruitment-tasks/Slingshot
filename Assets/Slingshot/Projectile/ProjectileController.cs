﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Slingshot
{
    public class ProjectileController : MonoBehaviour
    {
        public Rigidbody2D ProjectileRigidbody { get; set; }

        private void Awake()
        {
            ProjectileRigidbody = GetComponent<Rigidbody2D>();
        }

        public void MakeStatic() 
        {
            ProjectileRigidbody.bodyType = RigidbodyType2D.Static;
        }

        public void HurlProjectile(Vector2 vector2)
        {
            ProjectileRigidbody.bodyType = RigidbodyType2D.Dynamic;
            ProjectileRigidbody.AddForce(vector2, ForceMode2D.Impulse);
        }
    }
}