﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Slingshot
{
    public class ProjectilePoolManager : MonoBehaviour
    {
        [SerializeField]
        private ProjectileController projectilePrefab;
        [SerializeField]
        private int poolSize;

        // Inavtive objects ready for pulling
        private Queue<ProjectileController> projectilePool = new Queue<ProjectileController>();
        // Chronological references to pulled objects, in case poll gets emptied
        private Queue<ProjectileController> spawnedObjects = new Queue<ProjectileController>();

        private void Awake()
        {
            for (int i = 0; i < poolSize; ++i)
            {
                ProjectileController newProjectileInstance = Instantiate(projectilePrefab, transform);
                newProjectileInstance.gameObject.SetActive(false);

                projectilePool.Enqueue(newProjectileInstance);
            }
        }

        public ProjectileController GetProjectileFromPool()
        {
            ProjectileController pulledProjectile;

            if (projectilePool.Count > 0)
            {
                // get first object from pool
                pulledProjectile = projectilePool.Dequeue();
                pulledProjectile.gameObject.SetActive(true);
                spawnedObjects.Enqueue(pulledProjectile);
            }
            else
            {
                // get last spawned object and move it to back of queue
                pulledProjectile = spawnedObjects.Dequeue();
                spawnedObjects.Enqueue(pulledProjectile);
            }

            return pulledProjectile;
        }

        // Manual call to put object back to pool
        public void ReturnProjectileToPool()
        {
            ProjectileController returnedProjectile = spawnedObjects.Dequeue();
            returnedProjectile.gameObject.SetActive(false);
            projectilePool.Enqueue(returnedProjectile);
        }
    }
}