﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Slingshot
{
    public class CameraController : MonoBehaviour
    {
        public static CameraController Instance;

        public bool FollowTarget { get; set; } = true;
        [SerializeField]
        private Transform currentTarget;
        public Transform CurrentTarget { get => currentTarget; private set => currentTarget = value; }

        private Vector3 followVelocity;
        private float smoothTime = 0.1f;

        private void Awake()
        {
            if (Instance != null)
            {
                throw new System.Exception("Attempting to use mutliple " + typeof(CameraController).ToString() + " singleton!");
            }

            Instance = this;
        }

        private void Update()
        {
            if (FollowTarget) 
            {
                transform.position = Vector3.SmoothDamp(transform.position, new Vector3(CurrentTarget.position.x, CurrentTarget.position.y, transform.position.z), ref followVelocity, smoothTime);
            }
        }

        public void SwitchFollowTarget(Transform newTarget) 
        {
            CurrentTarget = newTarget;
        }
    }
}