﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Slingshot.Input
{
    public class InputManager : MonoBehaviour
    {
        public static InputManager Instance { get; set; }

        public InputActions InputActions { get; set; }

        private void Awake()
        {
            if (Instance != null)
            {
                throw new System.Exception("Attempting to use mutliple " + typeof(InputManager).ToString() + " singleton!");
            }

            Instance = this;
            InputActions = new InputActions();
            InputActions.Enable();
        }
    }
}